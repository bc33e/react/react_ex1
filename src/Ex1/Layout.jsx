import React, { Component } from "react";
import Banner from "./Banner";
import Content from "./Content";

import Footer from "./Footer";
import Header from "./Header";

export default class Layout extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <Content />
        <Footer />
      </div>
    );
  }
}
